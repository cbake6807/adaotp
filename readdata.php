<?php

require("vendor/autoload.php");

use lepiaf\SerialPort\SerialPort;
use lepiaf\SerialPort\Parser\SeparatorParser;
use lepiaf\SerialPort\Configure\TTYConfigure;

$con = mysqli_connect("localhost","root","root");
if (!$con)
  {
  die('Could not connect: ' . mysql_error());
  }
mysqli_select_db($con, "adaotp");

//change baud rate
$configure = new TTYConfigure();
$configure->removeOption("9600");
$configure->setOption("115200");

$serialPort = new SerialPort(new SeparatorParser("\n"), $configure);

try{
  $serialPort->open("/dev/ttyUSB0");
}catch (lepiaf\SerialPort\Exception\DeviceNotFound $e){
  echo $e->getMessage();
}

$addMoreCountOnce = true;

while ($data = $serialPort->read()) {

	$arr = str_getcsv($data);
	mysqli_query($con, "INSERT INTO sensor_logs (created, temp1, current1, current2, pressure1) VALUES ('".date('Y-m-d H:i:s')."', '".$arr[1]."', '".$arr[10]."', '".$arr[13]."', '".$arr[16]."')");

//    [1] => 77.90
//    [4] => 62.32
//    [7] => 6068.41
//    [10] => -12736
//    [13] => 176
//    [16] => 17.6909732818

}

$serialPort->close();
